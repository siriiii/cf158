#include <stdio.h>
int main()
{
  int i, low, high, mid, n, search, A[50];

  printf("Enter number of elements\n");
  scanf("%d", &n);

  printf("Enter %d integers\n", n);

  for (i = 0; i < n; i++)
    scanf("%d", &A[i]);

  printf("Enter value to find\n");
  scanf("%d", &search);

  low = 0;
  high = n - 1;
  mid = (low+high)/2;

  while (low <= high)
  {
    if (A[mid] < search)
      low = mid + 1;
    else if (A[mid] == search) 
        {
      printf("%d found at location %d.\n", search, mid+1);
      break;
    }
    else
      high = mid- 1;

    mid = (low + high)/2;
  }
  if (low > high)
    printf("Not found %d isn't present in the list.\n", search);

  return 0;
}