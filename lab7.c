#include <stdio.h>

int main() 
{ 

    char s1[100] = "Avengers-", s2[100] = "Endgame"; 
    char s3[100]; 
    int i = 0, j = 0; 
    printf("\nFirst string: %s", s1); 

    printf("\nSecond string: %s", s2); 

    while (s1[i] != '\0') { 

        s3[j] = s1[i]; 

        i++; 

        j++; 

    } 

    i = 0; 

    while (s2[i] != '\0') { 

        s3[j] = s2[i]; 

        i++; 

        j++; 

    } 

    s3[j] = '\0'; 

    printf("\nConcatenated string: %s\n", s3); 

    return 0;
}